module Connection = {
  type t = {
    url: string,
    id: string,
    headers: Js.Dict.t(string),
    deadCount: int,
    resurrectTimeout: int,
    openRequests: int,
    status: string,
    roles: Js.Dict.t(bool),
  };
};

module Request = {
  module Params = {
    type t = {
      method_: string,
      path: string,
      body: option(string),
      querystring: string,
      headers: Js.Dict.t(string),
      timeout: int,
    };

    let make = (method_, path, body, querystring, headers, timeout) => {
      method_,
      path,
      body,
      querystring,
      headers,
      timeout,
    };

    let decode = json =>
      Decode.AsResult.OfParseError.Pipeline.(
        succeed(make)
        |> field("method", string)
        |> field("path", string)
        |> optionalField("body", string)
        |> field("querystring", string)
        |> field("headers", dict(string))
        |> field("timeout", intFromNumber)
        |> run(json)
      );
  };

  type t = {
    id: int,
    options: Js.Json.t,
    params: Params.t,
  };
};

module Meta = {};

module Health = {
  type t = {
    body: string,
    statusCode: int,
    headers: Js.Dict.t(string),
  };
};
