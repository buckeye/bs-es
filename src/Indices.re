module Create = {
  module Settings = {
    [@bs.deriving abstract]
    type t = {index: Index_settings.t};
  };
  module Body = {
    [@bs.deriving abstract]
    type t = {
      mappings: Js.Json.t,
      settings: Settings.t,
    };
  };

  module Options = {
    [@bs.deriving abstract]
    type t = {
      // The name of the index.
      index: string,
      // Whether a type should be expected in the body of the mappings.
      // https://www.elastic.co/guide/en/elasticsearch/reference/6.8/indices-create-index.html
      // Type names are being removed from Elasticsearch 7.0+, therefore,
      // we default this setting to `false`. Meaning the `mappings` element
      // will no longer take the type name as a top-level key by default.
      [@bs.optional]
      includeTypeName: bool,
      // Set the number of active shards to wait for before the
      // operation returns.
      [@bs.optional]
      waitForActiveShards: int,
      // Explicit operation timeout.
      [@bs.optional]
      timeout: int,
      // Specify a timeout for connection to master
      [@bs.optional]
      masterTimeout: int,
      // Whether to update the mapping for all fields with the same name
      // across all types or not.
      [@bs.optional]
      updateAllTypes: bool,
    };
  };
};
