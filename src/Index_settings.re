// @TODO implement the `analysis` field, this will be tricky because
// it can take arbitrary strings as functions in some cases.
[@bs.deriving abstract]
type t = {
  // ### Static Settings
  // The number of primary shards than an index should have.
  [@bs.optional]
  number_of_shards: int, // defaults to 5, max 1024
  // Whether or not shards should be checked for corruption before opening.
  // When corruption is detected, it will prevent the shard from being
  // opened.
  // Accepts:
  //   `false` - (default) Don't check for corruption when opening a shard.
  //   `checksum` - Check for physical corruption.
  //   `true` - Check for both physical and logical corruption.
  //   `fix` - (deprecated)
  [@bs.optional]
  check_on_startup: string,
  // The `default` value compresses stored data with LZ4 compression, but
  // this can be set to `best_compression` which uses `DEFLATE` for a
  // higher compression ratio at the expense of slower stored fields
  // performance.
  // Accepts: `default` | `best_compression` | `force_merge`
  [@bs.optional]
  codec: string,
  // The number of shards a customer routing value can go to.
  [@bs.optional]
  routing_partition_size: int, // defaults to 1
  // Indicates whether cached filters are pre-loaded for nested queries.
  [@bs.optional]
  load_fixed_bitset_filters_eagerly: bool, // defaults to `true`.
  // ### Dynamic Settings
  // The followig settings can be updated dynamically after the index
  // has been created.
  // The number of replicas each primary shard has.
  [@bs.optional]
  number_of_replicas: int, // defaults to 1
  // Auto-expand the number of replicas based on the number of data nodes
  // in the cluster.  Set to a dash delimited lower and upper bound
  //(e.g. `0-5`) or use `all` for the upper bound (e.g. `0-all`).
  // Defaults to `false` (i.e disabled).
  [@bs.optional]
  auto_expand_replicas: string,
  // How often to perform a refresh operation, which makes recent changes to
  // the index visible to search. Defaults to `1s`. Can be set to `-1` to
  // disable refresh.
  [@bs.optional]
  refresh_interval: string,
  // The maximum value of `from + size` for searches to this index. Defaults
  // to `10000`.  Search requests take heap memory and time proportional to
  // `from + size` and this limits that memory.
  [@bs.optional]
  max_result_window: int,
  // The maximum value of `from + size` for inner hits definition and top
  // hits aggregations to this index. Defaults to `100`.  Inner hits and top
  // hits aggregation take heap memory and time proportional to `from + size`
  // and this limits that memory.
  [@bs.optional]
  max_inner_result_window: int,
  // The maximum value of `window_size` for `rescore` requests in searches of
  // this index.  Defaults to `index.max_result_window` which defaults to
  // `10000`.  Search requests take heap memory and time proportional to
  // `max(window_size, from + size)` and this limits that memory.
  [@bs.optional]
  max_rescore_window: int,
  // The maximum number of `docvalue_fields` that are allowed in a query.
  // Defaults to `100`.  Doc value fields are costly since they might incur
  // a per-field per-document seek.
  [@bs.optional]
  max_docvalue_fields_search: int,
  // The maximum number of `script_fields` that are allowed in a query.
  // Defaults to `32`.
  [@bs.optional]
  max_script_fields: int,
  // The maximum allowed difference between `min_gram` and `max_gram` for
  // `NGramTokenizer` and `NGramTokenFilter`. Defaults to `1`.
  [@bs.optional]
  max_ngram_diff: int,
  // The maximum allowed difference between `max_shingle_size` and
  // `min_shingle_size` for `ShingleTokenFilter`. Defaults to `3`
  [@bs.optional]
  max_shingle_diff: int,
  // Set to `true` to make the index and index metadata read-only, `false` to
  // allow writes and metadata changes.
  [@bs.optional] [@bs.as "blocks.read_only"]
  blocks_read_only: bool,
  // Identical to `index.blocks.read_only` but allows deleting the index to
  // free up resources.
  [@bs.optional] [@bs.as "blocks.read_only_allow_delete"]
  blocks_read_only_allow_delete: bool,
  // Set to `true` to disable read operations against the index.
  [@bs.optional] [@bs.as "blocks.read"]
  blocks_read: bool,
  // Set to `true` to disable data write operations agains the index.
  // Unlike `read_only`, this setting does not affect metadata.  For instance
  // you can close an index with a `write` block, but not an index with
  // a `read_only` block.
  [@bs.optional] [@bs.as "blocks.write"]
  blocks_write: bool,
  // Set to `true` to disable  index metadata reads and writes.
  [@bs.optional] [@bs.as "blocks.metadata"]
  blocks_metadata: bool,
  // Maximum number of refresh listeners available on each shard of the index.
  // These listeners are used to implement `refresh=wait_for`
  [@bs.optional]
  max_refresh_listeners: int,
  // The maximum number of characters that will be analyzed for a highlight
  // request.  This setting is only applicable when highlighting is requested
  // on a text that was indexed without offsets or term vectors.
  // Defaults to `-1`
  [@bs.optional] [@bs.as "highlight.max_analyzed_offset"]
  highlight_max_analyzed_offset: int,
  // The maximum number of terms that can be used in `Terms Query`.
  // Defaults to `65536`.
  [@bs.optional]
  max_terms_count: int,
  // Controls shard allocation for this index. Accepts:
  // * `all` _(default)_ - Allows shard allocation for all shards.
  // * `primaries` - Allows shard allocation only for primary shards.
  // * `new_primaries` - Allows shard allocation only for newly-created
  //                     primary shards.
  // * `none` - No shard allocation is allowed.
  [@bs.optional] [@bs.as "routing.allocation.enable"]
  routing_allocation_enable: string,
  // Enables shard rebalance for this index. Accepts:
  // * `all` _(default)_ - Allows shard rebalancing for all shards.
  // * `primaries` - Allows shard rebalancing only for primary shards.
  // * `replicas` - Allows shard rebalancing only for replica shards.
  // * `none` - No shard rebalancing is allowed.
  [@bs.optional] [@bs.as "routing.rebalance.enable"]
  routing_rebalance_enable: string,
  // The length of time that a deleted docdument's version number remains
  // available for further versioned operations. Defaults to `60s`
  [@bs.optional]
  gc_deletes: string,
  // The maximum length of regex that can be used in RegExp Query.
  // Defauls to `1000`
  [@bs.optional]
  max_regex_length: int,
  // the default ingest node pipeline for this index.
  [@bs.optional]
  default_pipeline: string,
  // ### Settings related to mappings
  // The maximum number of fields in an index. Field and object mappings, as
  // well as field aliases count towards this limit. Defaults to `1000`
  [@bs.optional] [@bs.as "mapping.total_fields.limit"]
  mapping_total_fields_limit: int,
  // The maximum depth for a field, which is measured as the numbr of inner
  // objects.  For instance, if all fields are defined at the root level,
  // then the depth is `1`.  If there is one object mapping, then the depth
  // is `2`, etc.  Defaults to `20`
  [@bs.optional] [@bs.as "mapping.depth.limit"]
  mapping_depth_limit: int,
  // The maximum number of distinct nested mappings in an index.
  // Defaults to `50`
  [@bs.optional] [@bs.as "mapping.nested_fields.limit"]
  mapping_nested_fields_limit: int,
};

let make = t;
