open Relude.Globals;

module Option = {
  let jsonBoolOrNull = t =>
    t |> Option.map(RJs.Json.fromBool) |> Option.getOrElse(RJs.Json.null);

  let jsonStringOrNull = t =>
    t |> Option.map(RJs.Json.fromString) |> Option.getOrElse(RJs.Json.null);

  let jsonStringCSVOrNull = t =>
    t
    |> Option.map(Relude.Extensions.Array.String.joinWith(","))
    |> jsonStringOrNull;
};
