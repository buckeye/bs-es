open Relude.Globals;

module Options = {
  [@bs.deriving abstract]
  type t = {node: string};
};

type t;

[@bs.module "@elastic/elasticsearch"] [@bs.new]
external make: Options.t => t = "Client";

let make = (~node: string) => make(Options.t(~node));

module Cat = {
  [@bs.send] [@bs.scope "cat"]
  external health: (t, Js.Json.t) => Js.Promise.t(string) = "health";

  module Format = {
    let toString =
      fun
      | `json => "json"
      | `yaml => "yaml";

    let toJson = t => t |> toString |> RJs.Json.fromString;
  };

  // ***NOTE** The elasticsearch documentation lies about the
  // [local] and [master_timeout] parameters, when I send them
  // the server complains.
  let health =
      // A short version of the Accept header, e.g. json, yaml.
      (
        ~format=`json,
        /*
         // Return local information, do not retrieve the stae from master node.
         // Defaults to `false
         ~local=false,
         // Explicit operation timeout for connection to master node.
         ~master_timeout: option(string)=?,
         */
        // Comma-separated list of column names to display.
        ~headers: option(array(string))=?,
        // Return help information.
        ~help=?,
        // Comma separated list of column names or column aliases to sort by
        ~sort: option(array(string))=?,
        // Set to false to disable timestamping. Defaults to `true`
        ~timestamp=true,
        // Verbose mode, display column headers.
        ~verbose=false,
        t,
      ) => {
    let input =
      RJs.(
        Json.fromListOfKeyValueTuples([
          ("format", format |> Format.toJson),
          /*
           ("local", local |> Json.fromBool),
           (
             "master_timeout",
             master_timeout |> Utility.Option.jsonStringOrNull,
           ),
           */
          ("h", headers |> Utility.Option.jsonStringCSVOrNull),
          ("help", help |> Utility.Option.jsonBoolOrNull),
          ("s", sort |> Utility.Option.jsonStringCSVOrNull),
          ("ts", timestamp |> Json.fromBool),
          ("v", verbose |> Json.fromBool),
        ])
      );

    RJs.Promise.toIOLazy(() => health(t, input));
  };
};
