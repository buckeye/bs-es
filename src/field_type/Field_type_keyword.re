open Relude.Globals;

module Mapping = {
  let make =
      // Mappping field-level query time boostin. Accepts a floating point
      // number, defaults to `1.0`
      (
        ~boost=1.0,
        // Should the field be stored on disk in a column-stride fashion,
        // so that it can later be used for sorting, aggregations or scripting?
        // Accepts `true` (default) or `false`
        ~doc_values=true,
        // Should global ordinal be loaded eagerly on refresh?
        // Accepts `true` or `false` (default). Enabling this is a good idea on
        // fields that are frequently used for terms aggregations.
        ~eager_global_ordinals=false,
        // Multi-fields allow the same string value to be indexed in multiple
        // ways for different purposes, such as one field for search and a
        // multi-field for sorting and aggregations.
        ~fields: option(Js.Json.t)=?,
        // Do not index any string longer than this value.  Defaults to
        // `2147483647` so that all values would be accepted.  However, please
        // that default dynamic mapping rules create a sub `keyword` field that
        // ooverrides this default by setting `ignore_above: 256`
        ~ignore_above: option(int)=?,
        // Should the field be searchable? Accepts `true` (default) or `false`.
        ~index=true,
        //W What information should be stored in the index, for scoring purposes.
        // Defaults to `docs` but can also be set to `freqs` to take term
        // frequency into account when computing scores.
        ~index_options=`docs,
        // Whether field-length should be taken into account when scoring
        // queries.  Accepts `true` or `false` (default).
        ~norms=false,
        // Accepts a string value which is substituted for any explicit `null`
        // values.  Defaults to `null`, which means the field is treated as
        // missing.
        ~null_value: option(string)=?,
        // Whether the field value should be stored and retrievable separately
        // from the `_source` field. Accepts `true` or `false` (default).
        ~store=false,
        // Which sotring algorithm or _similarity_ should be used. Defaults to
        // `BM25`
        ~similarity=`bm25,
        // How to pre-process the keyword prior to indexing. Defaults to `null`,
        // meaning the keyword is kept as-is.
        // @TODO - determine if there's some way to make this statically
        // verifiable. i.e. Does the specified normalizer actually exist?
        ~normalizer: option(string)=?,
        // Whether full text queries should split the input on whitespace when
        // building a query for this field.  Accepts `true` or `false` (default).
        ~split_queries_on_whitespace=false,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "keyword" |> Json.fromString),
        ("boost", boost |> Json.fromFloat),
        ("doc_values", doc_values |> Json.fromBool),
        ("eager_global_ordinals", eager_global_ordinals |> Json.fromBool),
        ("fields", fields |> Option.getOrElse(Json.null)),
        (
          "ignore_above",
          ignore_above
          |> Option.map(Json.fromInt)
          |> Option.getOrElse(Json.null),
        ),
        ("index", index |> Json.fromBool),
        (
          "index_options",
          index_options |> Field_type_setting.IndexOptions.toJson,
        ),
        ("norms", norms |> Json.fromBool),
        (
          "null_value",
          null_value
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        ("store", store |> Json.fromBool),
        ("similarity", similarity |> Field_type_setting.Similarity.toJson),
        (
          "normalizer",
          normalizer
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        (
          "split_queries_on_whitespace",
          split_queries_on_whitespace |> Json.fromBool,
        ),
      ])
    );
  };
};
