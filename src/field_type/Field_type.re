type t = Js.Json.t;

module Mapping = {
  let binary = Field_type_binary.Mapping.make;
  let boolean = Field_type_boolean.Mapping.make;
  let byte = Field_type_numeric.Mapping.Byte.make;
  let completion = Field_type_completion.Mapping.make;
  let date = Field_type_date.Mapping.make;
  let date_range = Field_type_range.Mapping.Date.make;
  let double = Field_type_numeric.Mapping.Double.make;
  let double_range = Field_type_range.Mapping.Double.make;
  let float = Field_type_numeric.Mapping.Float.make;
  let float_range = Field_type_range.Mapping.Float.make;
  let geo_point = Field_type_geo_point.Mapping.make;
  let half_float = Field_type_numeric.Mapping.HalfFloat.make;
  let integer = Field_type_numeric.Mapping.Integer.make;
  let integer_range = Field_type_range.Mapping.Integer.make;
  let ip = Field_type_ip.Mapping.make;
  let keyword = Field_type_keyword.Mapping.make;
  let long = Field_type_numeric.Mapping.Long.make;
  let long_range = Field_type_range.Mapping.Long.make;
  let nested = Field_type_nested.Mapping.make;
  let object_ = Field_type_object.Mapping.make;
  let scaled_float = Field_type_numeric.Mapping.ScaledFloat.make;
  let short = Field_type_numeric.Mapping.Short.make;
  let text = Field_type_text.Mapping.make;
  let token_count = Field_type_token_count.Mapping.make;
};
