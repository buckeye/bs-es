// Range datatypes

open Relude.Globals;

module Mapping = {
  module Abstract = (Impl: {let kind: string;}) => {
    let make =
        // Try to convert strings to numbers and truncate fractions for integers.
        // Accepts `true` (default) or `false`
        (
          ~coerce=true,
          // Mappping field-level query time boostin. Accepts a floating point
          // number, defaults to `1.0`
          ~boost=1.0,
          // Should the field be searchable? Accepts `true` (default) or `false`.
          ~index=true,
          // Whether the field value should be stored and retrievable separately
          // from the `_source` field. Accepts `true` or `false` (default).
          ~store=false,
          _,
        ) => {
      RJs.(
        Json.fromListOfKeyValueTuples([
          ("type", Impl.kind |> Json.fromString),
          ("coerce", coerce |> Json.fromBool),
          ("boost", boost |> Json.fromFloat),
          ("index", index |> Json.fromBool),
          ("store", store |> Json.fromBool),
        ])
      );
    };
  };

  // ### Integer Range
  // A range of signed 32-bit integers with a minimum value of `-2^31`
  // and a maximum of `2^31-1`.
  module Integer =
    Abstract({
      let kind = "integer_range";
    });

  // ### Float Range
  // A range of single-precision 32-bit IEEE 754 floating point values.
  module Float =
    Abstract({
      let kind = "float_range";
    });

  // ### Long Range
  // A range of signed 64-bit integers with a minimum value of `-2^63`
  // and a maximum of `2^63-1`.
  module Long =
    Abstract({
      let kind = "long_range";
    });

  // ### Double Range
  // A range of double-precision 64-bit IEEE 754 floating point values.
  module Double =
    Abstract({
      let kind = "double_range";
    });

  // ### Date Range
  // A range of values represented as an unsigned 64-bit integer, number of
  // milliseconds elapsed since system epoch.
  module Date =
    Field_type_date_abstract.Mapping.Make({
      let kind = "date_range";
    });
};
