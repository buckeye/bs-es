// ## Completion Suggester
// The `completion` suggester provides auto-complete/search-as-you-type
// functionality. Theis is a navigational feature to guide users to relevant
// results as they are typing, improving search precision.  It is not meant
// for spell correction or did-you-'mean functionality like the `term` or
// `phrase` suggesters.
// Ideally, auto-complete functionality should be as fast as a user types
// to provide instant feedback relevant to what a user has already typed in.
// Hence, `completion` suggester is optimized for speed. The suggester uses
// data structures that enable fast lookups, but are costly to build and are
// stored in-memory.
open Relude.Globals;

module Mapping = {
  let make =
      // The index analyzer to use, defaults to `simple`
      (
        ~analyzer="simple",
        // The search analyzer to use, defaults to the value of `analyzer`
        ~search_analyzer=?,
        // Preserves the separators, defaults to `true`. If disabled, you could
        // find a field starting with `Foo Fighters`, if you suggest for `foof`.
        ~preserve_separators=true,
        // Enables position increments, defaults to `true`.  If disabled and using
        // stopwords analyzer, you could get a field startingg with `The Beatles`,
        // if you suggest for `b`. ***NOTE***: You could also achieve this by
        // indexing two inputs, `Beatles` and `The Beatles`, no need to change a
        // simple analyzer, if you are able to enrich your data.
        ~preserve_position_increments=true,
        // Limits the length of a single input, defaults to `50` UTF-16 code
        // points. This limit is only used at index time to reduce the total
        // number of characters per inputut string in order to prevent massive
        // inputs from bloating the underlying data-structure.  Most use cases
        // won't be influenced by the default value since prefix completions
        // seldom grow beyondd prefixes longer than a handful of characters.
        ~max_input_length=50,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "completion" |> Json.fromString),
        ("analyzer", analyzer |> Json.fromString),
        (
          "search_analyzer",
          search_analyzer |> Option.getOrElse(analyzer) |> Json.fromString,
        ),
        ("preserve_separators", preserve_separators |> Json.fromBool),
        (
          "preserve_position_increments",
          preserve_position_increments |> Json.fromBool,
        ),
        ("max_input_length", max_input_length |> Json.fromInt),
      ])
    );
  };
};
