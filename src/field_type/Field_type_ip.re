// ## IP datatype
// An `ip` field can index/store either [IPv4] or [IPv6] addresses.
open Relude.Globals;

module Mapping = {
  let make =
      // Mapping field-level query time boosting.
      // Accepts a floating point number, defaults to `1.0`
      (
        ~boost=1.0,
        // Should the field be stored on disk in a column-stride fashion,
        // so that it can later be used for sorting, aggregations or scripting?
        // Accepts `true` (default) or `false`
        ~doc_values=true,
        // Should the field be searchable? Accepts `true` (default) or `false`.
        ~index=true,
        // Accepts a string value which is substituted for any explicit `null`
        // values.  Defaults to `null`, which means the field is treated as
        // missing.
        ~null_value: option(string)=?,
        // Whether the field value should be stored and retrievable separately
        // from the `_source` field. Accepts `true` or `false` (default).
        ~store=false,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "ip" |> Json.fromString),
        ("boost", boost |> Json.fromFloat),
        ("doc_values", doc_values |> Json.fromBool),
        ("index", index |> Json.fromBool),
        (
          "null_value",
          null_value
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        ("store", store |> Json.fromBool),
      ])
    );
  };
};
