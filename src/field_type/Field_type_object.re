// ## Object datatype
// JSON documents are hierarchical in natur: the documentsment may contain
// inner objects which, in turn, may contain inner objects themselves.
open Relude.Globals;

module Mapping = {
  let make =
      // Whether or not `properties` should be added dynamically to an
      // existing nested object. Accepts `true` (default), `false` and
      // `strict`.
      (
        ~dynamic=`true_,
        // Whether the JSON value given for the object field should be parsed
        // and indexed (`true`, default) or completely ignored (`false`).
        ~enabled=true,
        // The fields within the nested object, which can be of any [datatype],
        // including `nested`. New properties may be added to an existing
        // nested object.
        ~properties,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "object" |> Json.fromString),
        ("dynamic", dynamic |> Field_type_setting.Dynamic.toJson),
        ("enabled", enabled |> Json.fromBool),
        ("properties", properties),
      ])
    );
  };
};
