// ## Token count datatype
// A field type of `token_count` is really an [integer] field which accepts
// storedring values, analyzes them, then indexes the number of tokens in
// the string.
open Relude.Globals;

module Mapping = {
  let make =
      // The [analyzesr] which should be used to analyze the string
      // value. Required. For best performance, use an analyzer without
      // token filters.
      (
        ~analyzer: string,
        // Indicates if position increments should be counted. Set to `false`
        // if you don't want to count tokens removed by analyzer filters
        // (like [stop]).  Defaults to `true`.
        ~enable_position_increments=true,
        // Mappping field-level query time boostin. Accepts a floating point
        // number, defaults to `1.0`
        ~boost=1.0,
        // Should the field be stored on disk in a column-stride fashion,
        // so that it can later be used for sorting, aggregations or scripting?
        // Accepts `true` (default) or `false`
        ~doc_values=true,
        // Should the field be searchable? Accepts `true` (default) or `false`.
        ~index=true,
        // Accepts a string value which is substituted for any explicit `null`
        // values.  Defaults to `null`, which means the field is treated as
        // missing.
        ~null_value: option(string)=?,
        // Whether the field value should be stored and retrievable separately
        // from the `_source` field. Accepts `true` or `false` (default).
        ~store=false,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "token_count" |> Json.fromString),
        ("analyzer", analyzer |> Json.fromString),
        (
          "enable_position_increments",
          enable_position_increments |> Json.fromBool,
        ),
        ("boost", boost |> Json.fromFloat),
        ("doc_values", doc_values |> Json.fromBool),
        ("index", index |> Json.fromBool),
        (
          "null_value",
          null_value
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        ("store", store |> Json.fromBool),
      ])
    );
  };
};
