open Relude.Globals;

module Dynamic = {
  let toString =
    fun
    | `true_ => "true"
    | `false_ => "false"
    | `strict => "strict";

  let toJson = t => t |> toString |> RJs.Json.fromString;
};

module FieldDataFrequencyFilter = {
  type t = {
    min: float,
    max: float,
    min_segment_size: int,
  };

  let make = (~min, ~max, ~min_segment_size) => {
    let (min, max) =
      switch (min, max) {
      | (min, max) when min > max =>
        failwith({j|FDF: max must be greater than the min|j})
      | tuple => tuple
      };

    {min, max, min_segment_size};
  };

  let toJson = ({min, max, min_segment_size}) =>
    RJs.Json.(
      fromListOfKeyValueTuples([
        ("min", min |> fromFloat),
        ("max", max |> fromFloat),
        ("min_segment_size", min_segment_size |> fromInt),
      ])
    );
};

module IndexOptions = {
  let toString =
    fun
    | `docs => "docs"
    | `freqs => "freqs"
    | `positions => "positions"
    | `offsets => "offsets";

  let toJson = t => t |> toString |> RJs.Json.fromString;
};

module IndexPrefixes = {
  type t = {
    min_chars: int,
    max_chars: int,
  };

  let make = (~min, ~max) => {
    let (min_chars, max_chars) =
      switch (min, max) {
      | (min, _) when min < 1 =>
        failwith({j|`min` must be greater than zero: $min|j})
      | (_, max) when max > 20 =>
        failwith({j|`max` must be less than 20: $max|j})
      | (min, max) when max < min =>
        failwith({j|`max` must be greater than min|j})
      | (min, max) => (min, max)
      };

    {min_chars, max_chars};
  };

  let toJson = ({min_chars, max_chars}) =>
    RJs.Json.(
      fromListOfKeyValueTuples([
        ("min_chars", min_chars |> fromInt),
        ("max_chars", max_chars |> fromInt),
      ])
    );
};

module Similarity = {
  let toString =
    fun
    // The Okapi BM25 algorithm.  The algorithm used by default in Elasticsearch
    // and Lucene.  See [Pluggable Similarity Algorithms] for more information.
    | `bm25 => "BM25"
    // The TF/IDF algorithm which used to be the default in Elasticsearch and
    // Lucene.  See [Lucene's Practical Scoring Function] for more information.
    | `classic => "classic"
    | `tfidf => "classic"
    // A simple boolean similarity, which is used when full-text ranking is not
    // needed and the score should only be based on whether the query terms
    // match or not.  Boolean similarity gives terms a score equal to their
    // query boost.
    | `boolean => "boolean";
  // [Pluggable Similarity Algorithms]: https://www.elastic.co/guide/en/elasticsearch/guide/2.x/pluggable-similarites.html
  // [Lucene's Practical Scoring Function]: https://www.elastic.co/guide/en/elasticsearch/guide/2.x/practical-scoring-function.html

  let toJson = t => t |> toString |> RJs.Json.fromString;
};

module TermVector = {
  let toString =
    fun
    | `no => "no" // No term vectors are stored. (default)
    | `yes => "yes" // Just the terms in the field are stored.
    | `with_positions => "with_positions" // Terms ans positions are stored.
    | `with_offsets => "with_offsets" // Terms and character offsets are stored.
    // Terms, positions and character offsets are stored.
    | `with_positions_offsets => "with_positions_offsets"
    // Terms, positions and payloads are stored.
    | `with_positions_payloads => "with_positions_payloads"
    // Terms, positions, offsets and payloads are stored.
    | `with_positions_offsets_payloads => "with_positions_offsets_payloads";

  let toJson = t => t |> toString |> RJs.Json.fromString;
};
