// ## Nested datatype
// The `nested` type is a specialised version of the [object] datatype
// that allows arrays of objects to be indexed in a way that they can be
// queried independently of each other.
//
// ### Using `nested` fields for arrays of objects
// If you need to index arrays of objects and to maintain the independence
// of each object in the array, you should used the `nested` datatype instead
// of the [object] datatype.  Internally, nested objects index each object
// in the array as a separate hidden document, meaning that each nested
// object can be queried independently of the others, with the [`nested query].
//
// Nested documents can be:
// * queried with the [`nested`] query.
// * analyzed with the [`nested`] and [`reverse_nested`] aggregations.
// * sorted with [`nested sorting`].
// * retrieved and highlighted with [nested inner hits].
open Relude.Globals;

module Mapping = {
  let make =
      // Whether or not `properties` should be added dynamically to an
      // existing nested object. Accepts `true` (default), `false` and
      // `strict`.
      (
        ~dynamic=`true_,
        // The fields within the nested object, which can be of any [datatype],
        // including `nested`. New properties may be added to an existing
        // nested object.
        ~properties,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "nested" |> Json.fromString),
        ("dynamic", dynamic |> Field_type_setting.Dynamic.toJson),
        ("properties", properties),
      ])
    );
  };
};
