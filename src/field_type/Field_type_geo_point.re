// ## Geo-point datatype
// Field of type `geo_point` accept latitude-longitude pairs, which can be
// used:
// * to find geo-points within a [bounding box], within a certain [distance]
//   of a central point, or within a [polygon].
// * to aggregate documents [geograpohically] or by [distance] from a central
//   point.
// * to integrate distance into a document's [relevance score].
// * to [sort] documents by distance.
open Relude.Globals;

module Mapping = {
  let make =
      // If `true`, malformed geo-points are ignored. If `false` (default),
      // malformed geo-points throw an exception and reject the whole document.
      (
        ~ignore_malformed=false,
        // If `true` (default) three dimension points will be accepted
        // (stored in source) but only latitude and longitude values will be
        // indexed;  the third dimension is ignored. If `false`, geo-points
        // containing any more than latitude and longitude (two dimensions)
        // values throw an exception and reject the whole document.
        ~ignore_z_value=true,
        // Accepts a geopoint value which is substituted for any explicit `null`
        // values.  Defaults to `null`, which means the field is treated as
        // missing.
        ~null_value: option(string)=?,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "date" |> Json.fromString),
        ("ignore_malformed", ignore_malformed |> Json.fromBool),
        ("ignore_z_value", ignore_z_value |> Json.fromBool),
        (
          "null_value",
          null_value
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
      ])
    );
  };
};
