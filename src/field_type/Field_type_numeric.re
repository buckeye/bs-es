// ## Numeric datatypes
open Relude.Globals;

module Mapping = {
  module Abstract = (Impl: {let kind: string;}) => {
    let make =
        // Try to convert strings to numbers and truncate fractions for integers.
        // Accepts `true` (default) or `false`
        (
          ~coerce=true,
          // Mappint field-level query time boosting.  Accepts a floating point number,
          // defaults to `1.0`
          ~boost=1.0,
          // Should the field be stored on disk in a column-stride fashion,
          // so that it can later be used for sorting, aggregations or scripting?
          // Accepts `true` (default) or `false`
          ~doc_values=true,
          // If `true`, malformed numbers are ignored. If `false` (default), malformed
          // numbers thorw an exception and reject the whole document.
          ~ignore_malformed=false,
          // Should the field be searchable? Accepts `true` (default) or `false`.
          ~index=true,
          // Accepts a string value which is substituted for any explicit `null`
          // values.  Defaults to `null`, which means the field is treated as
          // missing.
          ~null_value: option(string)=?,
          // Whether the field value should be stored and retrievable separately
          // from the `_source` field. Accepts `true` or `false` (default).
          ~store=false,
          _,
        ) => {
      RJs.(
        Json.fromListOfKeyValueTuples([
          ("type", Impl.kind |> Json.fromString),
          ("coerce", coerce |> Json.fromBool),
          ("boost", boost |> Json.fromFloat),
          ("doc_values", doc_values |> Json.fromBool),
          ("ignore_malformed", ignore_malformed |> Json.fromBool),
          ("index", index |> Json.fromBool),
          (
            "null_value",
            null_value
            |> Option.map(Json.fromString)
            |> Option.getOrElse(Json.null),
          ),
          ("store", store |> Json.fromBool),
        ])
      );
    };
  };

  // A signed 64-bit integer with a minimum value of `-2^63` and a maximum
  // value of `2^63 - 1`
  module Long =
    Abstract({
      let kind = "long";
    });

  // A signed 32-bit integber with a minimum value of `-2^63` and a maximum
  // value of `2^63 - 1`
  module Integer =
    Abstract({
      let kind = "integer";
    });

  // A signed 16-bit integer with a minimum value of `-32,768` and a maximum
  // value of `32,767`
  module Short =
    Abstract({
      let kind = "short";
    });

  // A signed 8-bit integer with a minimum value of `-128` and a maximum
  // value of `127`
  module Byte =
    Abstract({
      let kind = "byte";
    });

  // A double-precision 64-bit IEEE 754 floating point number, restricted to
  // finite values.
  module Double =
    Abstract({
      let kind = "double";
    });

  // A single-precision 64-bit IEE 754 floating point number, restricted to
  // finite values.
  module Float =
    Abstract({
      let kind = "float";
    });

  // A half-precision 16-bit IEEE 754 floating point number, restricted to
  // finite values.
  module HalfFloat =
    Abstract({
      let kind = "half_float";
    });

  // A finite floating point number that is backed by a `long`, scaled by
  // a fixed `double` scaling factor.
  module ScaledFloat =
    Abstract({
      let kind = "scaled_float";
    });
};
