// ## Abstract Date datatype.
// This allows the date and date_range types to share implementation.
open Relude.Globals;

module Mapping = {
  module Make = (Impl: {let kind: string;}) => {
    let make =
        // Mappping field-level query time boostin. Accepts a floating point
        // number, defaults to `1.0`
        (
          ~boost=1.0,
          // Should the field be stored on disk in a column-stride fashion,
          // so that it can later be used for sorting, aggregations or scripting?
          // Accepts `true` (default) or `false`
          ~doc_values=true,
          // The date format(s) that can be parsed. Defaults to:
          // `strict_date_optional_time||epoch_millis`
          ~format=[|"strict_date_optional_time", "epoch_millis"|],
          // The locale to use when parsing dates since months do not have
          // the same names and/or abbreviations in all languages.  The default is
          // the `ROOT` locale.
          ~locale: option(string)=?,
          // If `true`, malformed numbers are ignored. If `false` (default), malformed
          // numbers thorw an exception and reject the whole document.
          ~ignore_malformed=false,
          // Should the field be searchable? Accepts `true` (default) or `false`.
          ~index=true,
          // Accepts a string value which is substituted for any explicit `null`
          // values.  Defaults to `null`, which means the field is treated as
          // missing.
          ~null_value: option(string)=?,
          // Whether the field value should be stored and retrievable separately
          // from the `_source` field. Accepts `true` or `false` (default).
          ~store=false,
          _,
        ) => {
      RJs.(
        Json.fromListOfKeyValueTuples([
          ("type", "date" |> Json.fromString),
          ("boost", boost |> Json.fromFloat),
          ("doc_values", doc_values |> Json.fromBool),
          (
            "format",
            format |> Field_type_date_format.arrayToString |> Json.fromString,
          ),
          (
            "local",
            locale
            |> Option.map(Json.fromString)
            |> Option.getOrElse(Json.null),
          ),
          ("ignore_malformed", ignore_malformed |> Json.fromBool),
          ("index", index |> Json.fromBool),
          (
            "null_value",
            null_value
            |> Option.map(Json.fromString)
            |> Option.getOrElse(Json.null),
          ),
          ("store", store |> Json.fromBool),
        ])
      );
    };
  };
};
