// ## Binary datatype
// The `binary` type accepts a binary value as a [Base64] encoded string.
// The field is not stored by default and is not searchable.
//
// [Base64]: https://en.wikipedia.org/wiki/Base64

open Relude.Globals;

module Mapping = {
  let make =
      // Should the field be stored on disk in a column-stride fashion,
      // so that it can later be used for sorting, aggregations or scripting?
      // Accepts `true` (default) or `false`
      (
        ~doc_values=true,
        // Whether the field value should be stored and retrievable separately
        // from the `_source` field. Accepts `true` or `false` (default).
        ~store=false,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "binary" |> Json.fromString),
        ("doc_values", doc_values |> Json.fromBool),
        ("store", store |> Json.fromBool),
      ])
    );
  };
};
