open Relude.Globals;

module Mapping = {
  let make =
      (
        ~boost=1.0,
        ~eager_global_ordinals=false,
        ~fielddata=false,
        ~fielddata_frequency_filter:
           option(Field_type_setting.FieldDataFrequencyFilter.t)=?,
        ~fields: option(Js.Json.t)=?,
        ~index=true,
        ~index_options=`positions,
        ~index_prefixes=Field_type_setting.IndexPrefixes.make(~min=2, ~max=5),
        ~index_phrases=false,
        ~norms=true,
        ~position_increment_gap=100,
        ~store=false,
        ~search_analyzer: option(string)=?,
        ~search_quote_analyzer: option(string)=?,
        ~similarity=`bm25,
        ~term_vector=`no,
        _,
      ) => {
    RJs.(
      Json.fromListOfKeyValueTuples([
        ("type", "text" |> Json.fromString),
        ("boost", boost |> Json.fromFloat),
        ("eager_global_ordinals", eager_global_ordinals |> Json.fromBool),
        ("fielddata", fielddata |> Json.fromBool),
        (
          "fielddata_frequency_filter",
          switch (fielddata_frequency_filter) {
          | None => Json.null
          | Some(x) => Field_type_setting.FieldDataFrequencyFilter.toJson(x)
          },
        ),
        ("fields", fields |> Option.getOrElse(Json.null)),
        ("index", index |> Json.fromBool),
        (
          "index_options",
          index_options |> Field_type_setting.IndexOptions.toJson,
        ),
        (
          "index_prefixes",
          index_prefixes |> Field_type_setting.IndexPrefixes.toJson,
        ),
        ("index_phrases", index_phrases |> Json.fromBool),
        ("norms", norms |> Json.fromBool),
        ("position_increment_gap", position_increment_gap |> Json.fromInt),
        ("store", store |> Json.fromBool),
        (
          "search_analyzer",
          search_analyzer
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        (
          "search_quote_analyzer",
          search_quote_analyzer
          |> Option.map(Json.fromString)
          |> Option.getOrElse(Json.null),
        ),
        ("similarity", similarity |> Field_type_setting.Similarity.toJson),
        ("term_vector", term_vector |> Field_type_setting.TermVector.toJson),
      ])
    );
  };
};
