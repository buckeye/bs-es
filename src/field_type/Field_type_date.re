// ## Date datatype
// JSON doesn't have a date datatype, so dates in Elasticsearch can either be:
//  * strings container formatted dates, e.g. `"2015-01-01` or
//    `"2015/01/01 12:10:30"`
//  * a long number representing __milliseconds-since-the-epoch__
//  * an integer representing __seconds-since-the-epoch__.
//
// Internally, dates are converted to UTC *if the time-zone is specified) and
// stored as a long number representing the __milliseconds-since-the-epoch__.
//
// Queries on dates are internally converted to range queries on this long
// representation, and the result of aggregations and stored fields is
// converted back to a string depending on the date format that is associated
// with the field.
//
// Date formats can be customised, but if no `format` is specified then it
// uses the default:
// ```
// "strict_date_optional_time||epoch_millis"
// ```
// This means that it will accept dates with optional timestamps, which
// conform to the formats supported by `strict_date_optional_time` or
// __milliseconds-since-the-epoch__.
//
// ### Multiple date formats
// Multiple formats can be specified by separating them with `||` as a
// separator.  Each format will be tried in turn until a matching format is
// found.  The first format will be used to convert the
// __milliseconds-since-the-epoch__ value back into a string.

module Mapping = {
  include Field_type_date_abstract.Mapping.Make({
    let kind = "date";
  });
};
