open Relude.Globals;

Ava.Async.test(
  Ava.ava,
  "Should always pass",
  t => {
    Ava.Test.pass(t);
    Ava.Test.finish(t);
  },
);

Ava.Async.test(
  Ava.ava,
  "Should connect and be able to read the tagline",
  t => {
    let client = Client.make(~node="http://localhost:9200");

    Client.Cat.health(client)
    |> IO.unsafeRunAsync(result =>
         result
         |> Result.tapError(_ => Ava.Test.fail(t))
         |> Result.tapOk(_ => Ava.Test.pass(t))
         |> Result.tapOk(json => Js.Console.log3("\n\n", json, "\n\n"))
         |> Result.tap(_ => Ava.Test.finish(t))
         |> ignore
       );
  },
);
